# Melodiction

![nat's logo](/static/melodiction-logo.gif)

Nat Hsu, Jennings Zhang, Vera Kong, Nicole Danuwidjaja

HackNY - September 29, 2019

## Installation

Jennings has no clue how to use virtualenv.

## Inspiration
The songs we listen to are oftentimes inspired by the conversations we have every day. Through the words we convey, we can understand the inspiration behind songs within the countless number of human interactions we have. Thus, our team wanted to find a way to identify songs based on specific keywords so that people could discover songs that they could be attracted to based on their particular meaning.
 
## What it does
Melodiction is a web-enabled application that allows users to input a phrase/sentence and output a list of song recommendations based on their input.

## How we built it
Our team of four divided up Melodiction into primarily front-end and back-end subteams. We looked into using Microsoft Azure for their Text Analytics API to extract information from user-inputted sentences. We used Azure to retain relevant phrases from the keywords and then parsed the results into a JSON object that was fetched to the Spotify API, which was able to generate a list of recommended songs based on relevant keywords. We display the songs with an HTML carousel.

## Challenges we ran into
We arrived at HackNY extremely late (6 pm) and did not finalize our idea until after 10pm on Saturday (9/28/19). We were all running on little to no sleep as we worked on the project during the entire night. As a result, we felt that we were not at our utmost best shape due to the exhaustion with the delay from Boston to NYC.

## Accomplishments that we're proud of
We were proud that we were able to muster up the courage to come up with an intriguing idea that all of us agreed upon, and overall, our team is satisfied with the work we were able to accomplish at HackNY.

## What we learned
Our team was able to learn how to use APIs and connect different platforms together.

## What's next for Melodiction
Continuing to refine the output of the song recommendations and increasing generalizations of the keywords to enable greater contextual song feedback.

