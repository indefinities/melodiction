#!/usr/bin/env python3.7
import os
from flask import Flask, render_template, url_for, jsonify, request
import azure
import spotify

app = Flask(__name__, static_url_path='')
app.config['JSON_AS_ASCII'] = False


@app.route('/')
def root():
    return app.send_static_file('index.html')


@app.route('/q', methods=['GET'])
def sentence_analysis():
    """
    /q?sentence=Everyday+I+think+about+eating+cheesy+apple+pies.
    """
    data = request.args.get('sentence')
    if not data:
        return 'no sentence', 400
    data = azure.key_phrases(data)
    return jsonify(spotify.search(data))


if __name__ == "__main__":
    PORT = os.getenv('PORT', 8080)
    app.run(host='0.0.0.0', port=PORT, threaded=True)
    # printing local ip address might be good
    # eventually transferring this to custom domain (https://melodiction.tech)
