import os
from requests import post
import json
from dotenv import load_dotenv

load_dotenv()
AZURE_KEY = os.environ['AZURE_KEY']

def key_phrases(sentence, lang='en'):
    """
    :param sentence: string
    :return: list of strings
    """
    key_phrases_url = 'https://eastus.api.cognitive.microsoft.com/' \
                      'text/analytics/v2.1/keyPhrases'
    headers = {
        'Ocp-Apim-Subscription-Key': AZURE_KEY,
        'Content-Type': 'application/json',
        'Host': 'eastus.api.cognitive.microsoft.com'
    }
    body = {
        'documents': [
            {
                'language': lang,
                'id': '1',
                'text': sentence
            }
        ]
    }
    r = post(key_phrases_url, data=json.dumps(body), headers=headers)
    return r.json()['documents'][0]['keyPhrases']


if __name__ == '__main__':
    print(key_phrases('Everyday I think about eating cheesy apple pies.'))
