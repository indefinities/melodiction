"""
Singleton class for Spotify API
"""
import os
from requests import post, get
from base64 import b64encode
from dotenv import load_dotenv

load_dotenv()
AZURE_KEY = os.environ['AZURE_KEY']
CLIENT_ID = os.environ['SPOTIFY_CLIENT_ID']
CLIENT_SECRET = os.environ['SPOTIFY_CLIENT_SECRET']

def get_token():
    """
    https://developer.spotify.com/documentation/general/guides/authorization-guide/#client-credentials-flow
    :return: true if successful
    """
    headers = { 'Authorization': 'Basic ' + b64encode(f'{CLIENT_ID}:{CLIENT_SECRET}'.encode()).decode() }
    r = post('https://accounts.spotify.com/api/token', data={'grant_type': 'client_credentials'}, headers=headers)
    r.raise_for_status()
    return r.json()['access_token']


def search(words, resultType='track', songCount='5', marketCountry='US'):
    if isinstance(words, list):
        ' '.join(words)
    # maybe we should authenticate server-to-server once, then refresh the token?
    headers = { 'Authorization': 'Bearer ' + get_token() }

    qs = {
        'type': resultType,
        'q': words
    }
    if songCount:
        qs['limit'] = songCount
    if marketCountry:
        qs['market'] = marketCountry

    r = get('https://api.spotify.com/v1/search', headers=headers, params=qs)
    return r.json()
